const express = require("express");
const https = require("https");
const fs = require("fs");

const app = express();

const baseDir = `${__dirname}/build/`;

app.use(express.static(`${baseDir}`));

app.get("*", (req, res) => res.sendFile("index.html", { root: baseDir }));

const port = 4000;

// app.listen(port, () =>
//   console.log(`Servidor subiu com sucesso em http://localhost:${port}`)
// );
https
  .createServer(
    {
      key: fs.readFileSync("/etc/letsencrypt/live/oneer.app/privkey.pem"),
      cert: fs.readFileSync("/etc/letsencrypt/live/oneer.app/fullchain.pem"),
      passphrase: "Master1*"
    },
    app
  )
  .listen(port);
