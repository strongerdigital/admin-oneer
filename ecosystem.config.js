module.exports = {
  apps: [
    {
      name: "oneer-admin",
      script: "server.js",
      watch: true,
      ignore_watch: ["node_modules", "tmp", "database", "__test__", "public"]
    }
  ]
};
