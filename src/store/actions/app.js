export function setAppLoading(loading) {
  return dispatch => {
    if (loading === true) {
      dispatch(setLoading(loading));
    } else {
      setTimeout(() => {
        dispatch(setLoading(loading));
      }, 500);
    }
  };
}
export function showSuccessNotification() {
  return dispatch => {
    dispatch(setSuccessNotification(true));

    setTimeout(() => {
      dispatch(setSuccessNotification(false));
    }, 1000);
  };
}
export function handleMiniNavbar() {
  return dispatch => {
    dispatch(setMiniNavbar());
  };
}

export function setLoading(loading) {
  return {
    type: "LOADING",
    loading
  };
}
export function setMiniNavbar() {
  return {
    type: "SET_MININAV"
  };
}
export function setSuccessNotification(success) {
  return {
    type: "SET_SUCCESS_NOTIFICATION",
    success
  };
}
