let defaultState = {
  loading: false,
  mininav: false,
  success: false
};

const rootReducer = (state = defaultState, action) => {
  switch (action.type) {
    case "LOADING":
      return {
        ...state,
        loading: action.loading
      };
    case "SET_MININAV":
      return {
        ...state,
        mininav: state.mininav ? false : true
      };
    case "SET_SUCCESS_NOTIFICATION":
      return {
        ...state,
        success: action.success
      };
    default:
      return state;
  }
};

export default rootReducer;
