import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Main from "../pages/Main";
import { PrivateRoute } from "./helpers";
import LoginPage from "../pages/Login";
import Loading from "../components/Loading";
import AdminRoutes from "./adminRoutes";
function AppRouter() {
  return (
    <Router>
      <Loading />
      <Route path="/" exact component={Main} />
      <Route path="/login" exact component={LoginPage} />
      {/* <Route path="/admin" component={AdminRoutes} /> */}
      <PrivateRoute path="/admin" component={AdminRoutes} />
    </Router>
  );
}

export default AppRouter;
