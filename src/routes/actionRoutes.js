import React from "react";
import Dashboard from "../pages/Dashboard";
import AccessoriesView from "../pages/Accessories/view";
import AccessoriesCreate from "../pages/Accessories/create";
import AccessoriesEdit from "../pages/Accessories/edit";

//Categories
import CategoriesView from "../pages/Categories/view";
import CategoriesCreate from "../pages/Categories/create";
import CategoriesEdit from "../pages/Categories/edit";

//Messages
import MessagesView from "../pages/Messages/view";
import MessagesCreate from "../pages/Messages/create";
import MessagesEdit from "../pages/Messages/edit";

//Users
import UsersView from "../pages/Users/view";

export function actionRoutes({ match }) {
  let Component = Dashboard;
  const { page, action, id } = match.params;
  if (action === "view") {
    switch (page) {
      case "accessories":
        Component = AccessoriesView;
        break;
      case "categories":
        Component = CategoriesView;
        break;
      case "messages":
        Component = MessagesView;
        break;
      case "users":
        Component = UsersView;
        break;
      default:
    }
  } else if (action === "edit") {
    switch (page) {
      case "accessories":
        Component = AccessoriesEdit;
        break;
      case "categories":
        Component = CategoriesEdit;
        break;
      case "messages":
        Component = MessagesEdit;
        break;
      default:
    }
  }
  return <Component id={id} />;
}

export function createRoutes({ match }) {
  const { page } = match.params;
  let Component = Dashboard;
  switch (page) {
    case "accessories":
      Component = AccessoriesCreate;
      break;
    case "categories":
      Component = CategoriesCreate;
      break;
    case "messages":
      Component = MessagesCreate;
      break;
    default:
  }
  return <Component />;
}
