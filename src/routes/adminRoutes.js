import React from "react";

import { connect } from "react-redux";
import * as AppActions from "../store/actions/app";
import { actionRoutes, createRoutes } from "./actionRoutes";
import Styles from "../components/styles";
import Footer from "../components/footer";
import Menu from "../components/Menu";
import TopNavBar from "../components/topnavbar";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Dashboard from "../pages/Dashboard";
import Page from "../pages/Page";
import Categories from "../pages/Categories";
import Accessories from "../pages/Accessories";
import Messages from "../pages/Messages";
import Questions from "../pages/Questions";
import Slides from "../pages/Slides";
import Users from "../pages/Users";
import UseTerms from "../pages/UseTerms";

function Pages({ match }) {
  const { page } = match.params;
  switch (page) {
    case "page":
      return <Page />;
    case "categories":
      return <Categories />;
    case "accessories":
      return <Accessories />;
    case "messages":
      return <Messages />;
    case "questions":
      return <Questions />;
    case "slides":
      return <Slides />;
    case "users":
      return <Users />;
    case "useterms":
      return <UseTerms />;

    default:
      return <Dashboard />;
  }
}
function Admin({ match, history, app: { mininav } }) {
  const logout = async () => {
    await localStorage.removeItem("@oneer:user");
    history.push("/login");
  };

  return (
    <Router>
      <div
        id="wrapper"
        className={`body-small ${mininav ? "mini-navbar" : ""}`}
      >
        <Styles />
        <Menu />
        <div id="page-wrapper" className={`gray-bg`}>
          <TopNavBar logout={logout} />

          <Route path={`/admin/:page?`} exact component={Pages} />
          <Route
            path={`/admin/:page/:action/:id`}
            exact
            component={actionRoutes}
          />
          <Route path={`/admin/:page/:create`} exact component={createRoutes} />
          <Footer />
        </div>
      </div>
    </Router>
  );
}

export default connect(
  state => state,
  AppActions
)(Admin);
