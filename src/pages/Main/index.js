import React, { useEffect } from "react";

// import { Container } from './styles';

export default function Main({ history }) {
  useEffect(() => {
    localStorage.getItem("@oneer:user") && history.push("/admin");
    !localStorage.getItem("@oneer:user") && history.push("/login");
  }, [history]);
  return <div />;
}
