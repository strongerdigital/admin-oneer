import React, { useState, useEffect } from "react";
import PageTitle from "../../components/PageTitle";
import Table from "../../components/Table";

import { loadAccessories } from "../../services/providers/accessories";

export default function Ads() {
  const [items, setItems] = useState([]);
  useEffect(() => {
    loadAccessories().then(response => {
      setItems(response);
    });
  }, []);
  const headers = ["#", "Nome"];
  const fields = ["id", "name"];
  return (
    <div>
      <PageTitle title={"Acessórios"} create={"accessories"} />

      <div className="wrapper wrapper-content">
        <Table
          headers={headers}
          items={items}
          action={"accessories"}
          fields={fields}
        />
      </div>
    </div>
  );
}
