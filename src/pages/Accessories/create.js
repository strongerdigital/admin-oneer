import React from "react";
import PageTitle from "../../components/PageTitle";
import Form from "./form";

import { createAccessory } from "../../services/providers/accessories";

export default function AccessoriesCreate() {
  const handleSave = data => {
    createAccessory(data).then(posted => {
      if (posted.error) alert("Favor conferir todos os campos");
    });
  };
  return (
    <>
      <PageTitle title={"Criar"} list={"accessories"} />
      <div className="wrapper wrapper-content">
        <Form handleSave={handleSave} details={{}} />
      </div>
    </>
  );
}
