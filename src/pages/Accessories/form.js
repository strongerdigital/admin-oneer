import React, { useEffect, useState } from "react";

export default function Form({ details, handleSave }) {
  const [formData, setFormData] = useState({});

  useEffect(() => {
    setFormData(details);
  }, [details]);
  const handleSubmit = e => {
    e.preventDefault();

    handleSave(formData);
  };

  return (
    <div className="ibox float-e-margins">
      <div className="ibox-content">
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label>Titulo</label>
            <input
              className="form-control"
              value={formData.name || ""}
              onChange={e => {
                setFormData({
                  ...formData,
                  name: e.target.value
                });
              }}
            />
          </div>

          <div className="text-right">
            <button className="btn btn-success" type="submit">
              Salvar
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
