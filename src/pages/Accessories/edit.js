import React, { useEffect, useState } from "react";
import PageTitle from "../../components/PageTitle";

import Form from "./form";

import {
  loadAccessory,
  updateAccessory
} from "../../services/providers/accessories";

export default function AccessoriesEdit({ id }) {
  const [details, setDetails] = useState({});
  useEffect(() => {
    loadAccessory(id).then(response => {
      setDetails(response);
    });
  }, [id]);
  const handleSave = data => {
    updateAccessory(data).then(posted => {
      if (posted.error) alert("Houve um erro no cadastro");
    });
  };
  return (
    <>
      <PageTitle title={"Editar"} list={"accessories"} />
      <div className="wrapper wrapper-content">
        <Form details={details} handleSave={handleSave} />
      </div>
    </>
  );
}
