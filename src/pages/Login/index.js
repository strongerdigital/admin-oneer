import React, { useState } from "react";
import Styles from "../../components/styles";
import api from "../../services/api";

export default function LoginPage({ history }) {
  const [login, setLogin] = useState({});
  const [loading, setLoading] = useState(false);
  const handleChange = (value, type) => {
    switch (type) {
      case "email":
        setLogin({
          ...login,
          email: value
        });
        break;
      case "password":
        setLogin({
          ...login,
          password: value
        });
        break;

      default:
        break;
    }
  };
  const handleSubmit = async e => {
    e.preventDefault();
    if (!loading) {
      if (login.email && login.password) {
        setLoading(true);
        const logged = await api.post("auth/signin", {
          ...login,
          admin: true
        });

        if (logged.data.user) {
          await localStorage.setItem(
            "@oneer:user",
            JSON.stringify(logged.data.user)
          );
          await localStorage.setItem(
            "@oneer:token",
            JSON.stringify(logged.data.token)
          );
          setLoading(false);
          history.push("/admin");
        } else {
          setLoading(false);
          alert(logged.data.error);
        }
      }
    }
  };
  return (
    <>
      <Styles />
      <div className="loginColumns animated fadeInDown">
        <div className="row">
          <div className="col-md-6 col-md-offset-3">
            <div className="ibox-content text-center">
              <img
                src={`${process.env.PUBLIC_URL}/images/logo.png`}
                alt="logo stronger"
                width="200"
              />
              <br />
              <br />
              <form className="m-t" onSubmit={handleSubmit}>
                <div className="form-group">
                  <input
                    value={login.email || ""}
                    type="email"
                    onChange={e => {
                      handleChange(e.target.value, "email");
                    }}
                    className="form-control"
                    placeholder="Email"
                    name="email"
                  />
                </div>
                <div className="form-group">
                  <input
                    value={login.password || ""}
                    onChange={e => {
                      handleChange(e.target.value, "password");
                    }}
                    type="password"
                    className="form-control"
                    placeholder="Senha"
                    name="password"
                  />
                </div>
                <button
                  type="submit"
                  className="btn btn-primary block full-width m-b"
                >
                  {loading ? <i className="fa fa-spinner fa-spin" /> : "Login"}
                </button>

                {/* <a href="# ">
                  <small>Esqueceu a senha? </small>
                </a> */}
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
