import React from "react";
import PageTitle from "../../components/PageTitle";
import Form from "./form";
import { createCategories } from "../../services/providers/categories";

export default function CategoriesCreate() {
  const handleSave = data => {
    createCategories(data).then(posted => {
      if (posted.error) alert("Favor conferir todos os campos");
    });
  };
  return (
    <>
      <PageTitle title={"Criar"} list={"categories"} />
      <div className="wrapper wrapper-content">
        <Form handleSave={handleSave} details={{}} />
      </div>
    </>
  );
}
