import React, { useEffect, useState } from "react";
import PageTitle from "../../components/PageTitle";

import Form from "./form";

import {
  loadCategory,
  updateCategory
} from "../../services/providers/categories";

export default function CategoriesEdit({ id }) {
  const [details, setDetails] = useState({});
  useEffect(() => {
    loadCategory(id).then(response => {
      setDetails(response);
    });
  }, [id]);
  const handleSave = data => {
    updateCategory(data).then(posted => {
      if (posted.error) alert("Houve um erro no cadastro");
    });
  };
  return (
    <>
      <PageTitle title={"Editar"} list={"categories"} />
      <div className="wrapper wrapper-content">
        <Form details={details} handleSave={handleSave} />
      </div>
    </>
  );
}
