import React, { useState, useEffect } from "react";
import PageTitle from "../../components/PageTitle";
import Table from "../../components/Table";

import { loadCategories } from "../../services/providers/categories";

export default function Ads() {
  const [items, setItems] = useState([]);
  useEffect(() => {
    loadCategories().then(response => {
      setItems(response);
    });
  }, []);
  const headers = ["#", "Nome"];
  const fields = ["id", "title"];
  return (
    <div>
      <PageTitle title={"Categorias"} create={"categories"} />

      <div className="wrapper wrapper-content">
        <Table
          headers={headers}
          items={items}
          action={"categories"}
          fields={fields}
        />
      </div>
    </div>
  );
}
