import React, { useEffect, useState, useCallback } from "react";
import ImageUploader from "react-images-upload";

export default function Form({ details, handleSave }) {
  const [formData, setFormData] = useState({});
  const [photo, setPhoto] = useState({});
  useEffect(() => {
    setFormData(details);
  }, [details]);
  const handleSubmit = e => {
    e.preventDefault();
    const form = {
      ...formData,
      photo
    };

    handleSave(form);
  };
  const onDrop = useCallback(acceptedFiles => {
    acceptedFiles.forEach(file => {
      var reader = new FileReader();
      reader.onloadend = function(file) {
        const binaryStr = reader.result;
        setPhoto({ image: binaryStr, file: acceptedFiles });
      };
      reader.readAsDataURL(file);
    });
  }, []);

  return (
    <div className="ibox float-e-margins">
      <div className="ibox-content">
        <form onSubmit={handleSubmit}>
          <div className="form-group">
            <label>Titulo</label>
            <input
              className="form-control"
              value={formData.title || ""}
              onChange={e => {
                setFormData({
                  ...formData,
                  title: e.target.value
                });
              }}
            />
          </div>
          <ImageUploader
            withIcon={true}
            label="Esta será a imagem do seu anúncio"
            buttonText="Selecione a imagem"
            onChange={onDrop}
            imgExtension={[".jpg", ".gif", ".png", ".gif", ".jpeg"]}
            maxFileSize={5242880}
            withPreview={true}
            singleImage={true}
          />
          <div className="text-right">
            <button className="btn btn-success" type="submit">
              Salvar
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
