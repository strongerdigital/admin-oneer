import React, { useState, useEffect } from "react";
import PageTitle from "../../components/PageTitle";
import Table from "../../components/Table";

import { loadUsers } from "../../services/providers/users";

export default function Users() {
  const [items, setItems] = useState([]);
  useEffect(() => {
    loadUsers().then(response => {
      setItems(response);
    });
  }, []);
  const headers = ["#", "Nome", "Email"];
  const fields = ["id", "name", "email"];
  return (
    <div>
      <PageTitle title={"Usuários"} />
      <div className="row">
        <div className="wrapper wrapper-content">
          <Table
            headers={headers}
            items={items}
            action={"users"}
            fields={fields}
          />
        </div>
      </div>
    </div>
  );
}
