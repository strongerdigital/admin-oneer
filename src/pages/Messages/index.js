import React, { useState, useEffect } from "react";
import PageTitle from "../../components/PageTitle";
import Table from "../../components/Table";

import { loadMessages } from "../../services/providers/messages";

export default function Messages() {
  const [items, setItems] = useState([]);
  useEffect(() => {
    loadMessages().then(response => {
      setItems(response);
    });
  }, []);
  const headers = ["#", "Nome", "Telefone", "Email", "Assunto", "Texto"];
  const fields = ["id", "name", "phone", "email", "subject", "text"];
  return (
    <div>
      <PageTitle title={"Mensagens"} create={"messages"} />

      <div className="wrapper wrapper-content">
        <Table
          headers={headers}
          items={items}
          action={"messages"}
          fields={fields}
        />
      </div>
    </div>
  );
}
