import React, { useEffect, useState } from "react";
import PageTitle from "../../components/PageTitle";
import { format } from "date-fns";

import { loadMessage } from "../../services/providers/messages";

// import { Container } from './styles';

export default function MessagesView({ id }) {
  const [details, setDetails] = useState({});
  useEffect(() => {
    loadMessage(id).then(response => {
      setDetails(response);
    });
  }, [id]);

  return (
    <>
      <PageTitle
        title={"Detalhes"}
        edit={{ id, action: "messages" }}
        list={"messages"}
      />
      <div className="wrapper wrapper-content">
        <div className="col-md-6 col-md-offset-3">
          <div className="ibox float-e-margins">
            <div>
              <div className="ibox-content profile-content">
                <h4>{details.name}</h4>
                <p>{details.name}</p>
                <p>Criado em: {format(details.created_at, "DD/MM/YYYY")}</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
