import React from "react";
import PageTitle from "../../components/PageTitle";
import Form from "./form";

import { createMessage } from "../../services/providers/messages";

export default function MessagesCreate() {
  const handleSave = data => {
    createMessage(data).then(posted => {
      if (posted.error) alert("Favor conferir todos os campos");
    });
  };
  return (
    <>
      <PageTitle title={"Criar"} list={"messages"} />
      <div className="wrapper wrapper-content">
        <Form handleSave={handleSave} details={{}} />
      </div>
    </>
  );
}
