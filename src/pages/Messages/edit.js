import React, { useEffect, useState } from "react";
import PageTitle from "../../components/PageTitle";

import Form from "./form";

import { loadMessage, updateMessage } from "../../services/providers/messages";

export default function MessagesEdit({ id }) {
  const [details, setDetails] = useState({});
  useEffect(() => {
    loadMessage(id).then(response => {
      setDetails(response);
    });
  }, [id]);
  const handleSave = data => {
    updateMessage(data).then(posted => {
      if (posted.error) alert("Houve um erro no cadastro");
    });
  };
  return (
    <>
      <PageTitle title={"Editar"} list={"messages"} />
      <div className="wrapper wrapper-content">
        <Form details={details} handleSave={handleSave} />
      </div>
    </>
  );
}
