import React from "react";

// import { Container } from './styles';
import useCollapse from "react-collapsed";

export default function UseTerms({ item }) {
  const { getCollapseProps, getToggleProps, isOpen } = useCollapse();
  return (
    <>
      <div className="col-md-12">
        <div className="col-md-10">
          <h4>{item.title}</h4>
        </div>
        <div className="col-md-2">
          <button {...getToggleProps()}>
            {isOpen ? "Fechar" : "Detalhes"}
          </button>
        </div>
        <div className="col-md-12">
          <section {...getCollapseProps()}>{item.description}</section>
        </div>
      </div>
      <hr className="col-md-12" />
    </>
  );
}
