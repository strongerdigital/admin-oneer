import React, { useState, useEffect } from "react";
import PageTitle from "../../components/PageTitle";
import {
  updateUseTerm,
  loadUseTerms,
  createUseTermItem
} from "../../services/providers/useTerms";
import Item from "./Item";
// import { Container } from './styles';

export default function UseTerms() {
  const [formData, setFormData] = useState({});
  const [useTermItem, setUseTermItem] = useState({});
  const handleSubmit = async e => {
    e.preventDefault();

    await updateUseTerm(formData);
    load();
  };
  const handleSubmitItem = async e => {
    e.preventDefault();

    await createUseTermItem(useTermItem);
    setUseTermItem({ use_term_id: formData.id });
    load();
  };
  const load = () => {
    loadUseTerms().then(useterms => {
      if (useterms && useterms.id) {
        setFormData({
          description: useterms.description,
          items: useterms.items || []
        });
        setUseTermItem({
          use_term_id: useterms.id
        });
      }
    });
  };
  useEffect(() => {
    load();
  }, []);
  return (
    <div className="ibox float-e-margins">
      <div className="ibox-content">
        <PageTitle title={"Termos de uso"} />

        <div className="form-group">
          <form onSubmit={handleSubmit}>
            <br />
            <br />
            <label>Descrição principal</label>
            <textarea
              className="form-control"
              rows="10"
              value={formData.description || ""}
              onChange={e => {
                setFormData({
                  ...formData,
                  description: e.target.value
                });
              }}
            ></textarea>
            <div className="text-right">
              <button className="btn btn-success" type="submit">
                Salvar
              </button>
            </div>
          </form>
        </div>

        <div className="row">
          <div className="col-md-4">
            <h3>Cadastrar novo item</h3>
            <form onSubmit={handleSubmitItem}>
              <div className="form-group">
                <label>Titulo</label>
                <input
                  className="form-control"
                  value={useTermItem.title || ""}
                  onChange={e => {
                    setUseTermItem({
                      ...useTermItem,
                      title: e.target.value
                    });
                  }}
                />
              </div>
              <div className="form-group">
                <label>Descrição</label>
                <textarea
                  rows="20"
                  className="form-control"
                  value={useTermItem.description || ""}
                  onChange={e => {
                    setUseTermItem({
                      ...useTermItem,
                      description: e.target.value
                    });
                  }}
                />
              </div>
              <div className="text-right">
                <button className="btn btn-success" type="submit">
                  Salvar
                </button>
              </div>
            </form>
          </div>
          <div className="col-md-8">
            <div className="row">
              <h3>Items cadastrados</h3>
              {formData &&
                formData.items &&
                formData.items.map(item => <Item item={item} />)}
            </div>
          </div>
        </div>
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
      </div>
    </div>
  );
}
