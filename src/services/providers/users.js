import api from "../api";
import { store } from "../../index";
import {
  setAppLoading,
  showSuccessNotification
} from "../../store/actions/app";

export const loadUsers = async () => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`users`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const loadUser = async id => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`users/${id}`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const updateUser = async infos => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.put(`users/${infos.id}`, infos).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
      if (!data.error) store.dispatch(showSuccessNotification());
    });
  });
};
export const createuser = async infos => {
  store.dispatch(setAppLoading(true));

  return new Promise(function(resolve, reject) {
    api.post(`users`, infos).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
      if (!data.error) store.dispatch(showSuccessNotification());
    });
  });
};
