import api from "../api";
import { store } from "../../index";
import {
  setAppLoading,
  showSuccessNotification
} from "../../store/actions/app";

export const loadMessages = async () => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`messages`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const loadMessage = async id => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`messages/${id}`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const updateMessage = async infos => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.put(`messages/${infos.id}`, infos).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
      if (!data.error) store.dispatch(showSuccessNotification());
    });
  });
};
export const createMessage = async infos => {
  store.dispatch(setAppLoading(true));

  return new Promise(function(resolve, reject) {
    api.post(`messages`, infos).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
      if (!data.error) store.dispatch(showSuccessNotification());
    });
  });
};
