import api from "../api";
import { store } from "../../index";
import {
  setAppLoading,
  showSuccessNotification
} from "../../store/actions/app";

export const loadUseTerms = async () => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`useterms`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};

export const updateUseTerm = async infos => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.post(`useterms`, infos).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
      if (!data.error) store.dispatch(showSuccessNotification());
    });
  });
};
export const createUseTermItem = async infos => {
  store.dispatch(setAppLoading(true));

  return new Promise(function(resolve, reject) {
    api.post(`usetermsitem`, infos).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
      if (!data.error) store.dispatch(showSuccessNotification());
    });
  });
};
