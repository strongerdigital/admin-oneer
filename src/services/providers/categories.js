import api from "../api";
import { store } from "../../index";
import {
  setAppLoading,
  showSuccessNotification
} from "../../store/actions/app";
export const loadCategories = async () => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`categories`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const loadCategory = async id => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`categories/${id}`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};

export const updateCategory = async infos => {
  store.dispatch(setAppLoading(true));
  const uploadData = new FormData();
  if (infos.photo) {
    let element = infos.photo;
    if (element.file) {
      var targetPath = element.file[0];

      var fileName = element.file[0].name;

      uploadData.append("photo", targetPath, fileName);
    }
  }
  return new Promise(function(resolve, reject) {
    api.put(`categories/${infos.id}`, infos).then(({ data }) => {
      if (infos.photo && !data.error) {
        api.post(`categories/${data.id}/upload`, uploadData).then(() => {
          resolve(data);
          store.dispatch(setAppLoading(false));
          if (!data.error) store.dispatch(showSuccessNotification());
        });
      } else {
        resolve(data);
        store.dispatch(setAppLoading(false));
        if (!data.error) store.dispatch(showSuccessNotification());
      }
    });
  });
};
export const createCategories = async infos => {
  store.dispatch(setAppLoading(true));
  const uploadData = new FormData();
  if (infos.photo) {
    let element = infos.photo;
    if (element.file) {
      var targetPath = element.file[0];

      var fileName = element.file[0].name;

      uploadData.append("photo", targetPath, fileName);
    }
  }
  return new Promise(function(resolve, reject) {
    api.post(`categories`, infos).then(({ data }) => {
      if (infos.photo && !data.error) {
        api.post(`categories/${data.id}/upload`, uploadData).then(() => {
          resolve(data);
          store.dispatch(setAppLoading(false));
          if (!data.error) store.dispatch(showSuccessNotification());
        });
      } else {
        resolve(data);
        store.dispatch(setAppLoading(false));
        if (!data.error) store.dispatch(showSuccessNotification());
      }
    });
  });
};
