import api from "../api";
import { store } from "../../index";
import {
  setAppLoading,
  showSuccessNotification
} from "../../store/actions/app";
export const loadAccessories = async () => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`accessories`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const loadAccessory = async id => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.get(`accessories/${id}`).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
    });
  });
};
export const updateAccessory = async infos => {
  store.dispatch(setAppLoading(true));
  return new Promise(function(resolve, reject) {
    api.put(`accessories/${infos.id}`, infos).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
      if (!data.error) store.dispatch(showSuccessNotification());
    });
  });
};
export const createAccessory = async infos => {
  store.dispatch(setAppLoading(true));

  return new Promise(function(resolve, reject) {
    api.post(`accessories`, infos).then(({ data }) => {
      resolve(data);
      store.dispatch(setAppLoading(false));
      if (!data.error) store.dispatch(showSuccessNotification());
    });
  });
};
