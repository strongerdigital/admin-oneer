import axios from "axios";
import https from "https";

const API =
  process.env.NODE_ENV === "development"
    ? process.env.REACT_APP_DEV_API_URL
    : process.env.REACT_APP_PROD_API_URL;

const api = axios.create({
  baseURL: `${API}/api`,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
});
export default api;

// axios.defaults.baseURL = `${API}/api`;
// axios.httpsAgent = new https.Agent({
//   rejectUnauthorized: false
// });
// axios.interceptors.request.use(
//   async config => {
//     const token = await localStorage.getItem("@oneer:token");

//     if (token) {
//       config.headers.Authorization = "Bearer " + JSON.parse(token);
//     }
//     return config;
//   },
//   error => {
//     return Promise.reject(error);
//   }
// );

// const api = axios;
//  api;
