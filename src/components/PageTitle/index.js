import React from "react";
import { Link } from "react-router-dom";
// import { Container } from './styles';

export default function PageTitle({ title, create, edit, list }) {
  return (
    <div className="row wrapper border-bottom white-bg page-heading">
      <div className="col-md-10 col-lg-10 col-10 col-sm-10">
        <h2>{title || ""}</h2>
      </div>
      <div className="col-md-2 col-lg-2 col-2 col-sm-2">
        <br />
        {create && (
          <Link to={`/admin/${create}/create`} className="btn btn-primary">
            Novo
          </Link>
        )}
        {edit && (
          <Link
            to={`/admin/${edit.action}/edit/${edit.id}`}
            className="btn btn-primary"
          >
            Editar
          </Link>
        )}
        {list && (
          <Link to={`/admin/${list}/`} className="btn btn-primary">
            Ver todos
          </Link>
        )}
      </div>
    </div>
  );
}
