import React from "react";
import { store } from "../index";
import { handleMiniNavbar } from "../store/actions/app";

// import { Container } from './styles';

export default function components({ logout }) {
  const setMiniNavbar = () => {
    store.dispatch(handleMiniNavbar());
  };
  return (
    <div className="row border-bottom">
      <nav className="navbar navbar-static-top" role="navigation">
        <div className="navbar-header">
          <span>
            <button
              className="navbar-minimalize minimalize-styl-2 btn btn-primary"
              onClick={() => {
                setMiniNavbar();
              }}
            >
              <i className="fa fa-bars" />
            </button>
          </span>
        </div>
        <ul className="nav navbar-top-links navbar-right">
          <li>
            <a onClick={logout} href="# ">
              <i className="fa fa-sign-out" /> Sair
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
}
