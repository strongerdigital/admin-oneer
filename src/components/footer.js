import React from "react";

// import { Container } from './styles';

export default function components() {
  return (
    <footer>
      <div className="footer">
        <div className="text-left col-md-6">Área administrativa</div>
        <div className="text-right col-md-6">
          <img
            src={`${process.env.PUBLIC_URL}/images/logo.png`}
            alt="logo stronger"
            width="100"
          />
        </div>
      </div>
    </footer>
  );
}
