import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
// import { Container } from './styles';

export default function Table({
  headers,
  items,
  action,
  fields,
  noedit,
  nodelete
}) {
  const [list, setList] = useState([]);

  useEffect(() => {
    let i = {};
    items.forEach(item => {
      i = {};
      fields.forEach(element => {
        i[element] = item[element];
      });
      list.push(i);
    });
    setList(list);
  }, [fields, items, list]);
  return (
    <div className="col-lg-12">
      <div className="ibox float-e-margins">
        <div className="ibox-title">
          <h5>Listagem</h5>
        </div>
        <div className="ibox-content">
          <table className="table">
            <thead>
              <tr>
                {headers.map((header, key) => (
                  <th key={key}>{header}</th>
                ))}
                <th />
              </tr>
            </thead>
            <tbody>
              {list.map((item, key) => (
                <tr key={key}>
                  {fields.map((attr, key) => (
                    <td key={key}>{item[attr]}</td>
                  ))}

                  <td>
                    <ul className="pagination-sm pagination ng-isolate-scope ng-not-empty ng-valid">
                      <li className="pagination-page ng-scope">
                        <Link to={`/admin/${action}/view/${item.id}`}>
                          <i className="fa fa-eye" />
                        </Link>
                      </li>
                      {!noedit && (
                        <li className="pagination-page ng-scope ">
                          <Link to={`/admin/${action}/edit/${item.id}`}>
                            <i className="fa fa-pencil" />
                          </Link>
                        </li>
                      )}
                      {!nodelete && (
                        <li className="pagination-page ng-scope">
                          <a
                            href="# "
                            onClick={() => {
                              if (window.confirm("Deseja excluir este item?")) {
                                alert("Excluido com sucesso");
                              }
                            }}
                          >
                            <i className="fa fa-trash" />
                          </a>
                        </li>
                      )}
                    </ul>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}
