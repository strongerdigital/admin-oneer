const menuItens = [
  {
    title: "Acessórios",
    to: "accessories",
    icon: "file"
  },
  {
    title: "Categorias",
    to: "categories",
    icon: "file"
  },
  {
    title: "Mensagens",
    to: "messages",
    icon: "file"
  },
  {
    title: "Perguntas",
    to: "questions",
    icon: "file"
  },
  {
    title: "Slides",
    to: "slides",
    icon: "file"
  },
  {
    title: "Usuários",
    to: "users",
    icon: "file"
  },
  {
    title: "Termos de Uso",
    to: "useterms",
    icon: "file"
  }
];

export default menuItens;
