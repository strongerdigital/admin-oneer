import React from "react";

import { Link } from "react-router-dom";
import items from "./menuitems";
import MenuItem from "./MenuItem";

export default function Menu({ user }) {
  return (
    <nav className="navbar-default navbar-static-side" role="navigation">
      <div className="sidebar-collapse">
        <ul className="nav metismenu" id="side-menu">
          <li className="nav-header">
            <div className="profile-element">
              <span className="block m-t-xs font-bold text-white">
                Edilson Junior
              </span>
            </div>
            <div className="logo-element">STR</div>
          </li>
          <li>
            <Link to="/admin">
              <i className="fa fa-th-large" />
              <span className="nav-label ng-binding">Dashboard</span>
            </Link>
            {items.map((item, key) => (
              <MenuItem item={item} key={key} />
            ))}
          </li>
        </ul>
        {/* <img
          src={`${process.env.PUBLIC_URL}/images/logo-white.png`}
          alt="logo stronger"
          width="100"
        /> */}
      </div>
    </nav>
  );
}
