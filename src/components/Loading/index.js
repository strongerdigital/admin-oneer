import React from "react";
import { connect } from "react-redux";
import * as AppActions from "../../store/actions/app";
// import { Container } from './styles';

function Loading({ app: { loading } }) {
  return loading ? (
    <div className="loading-container visible active">
      <i className="fa fa-spinner fa-spin" />
    </div>
  ) : (
    <>{loading}</>
  );
}

export default connect(
  state => state,
  AppActions
)(Loading);
